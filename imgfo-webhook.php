<?php

/**
 * ImgFo Webhook Script PHP 
 * This script will allow https://imgfo.com to upload the content to your own webserver storage.
 *
 * @version 1.1.0
 * @license MIT
 */

// CONFIGURATION
$maxage = 31536000;                 // Max age of cache before expires (default is 1year = 31536000).
$dynamic_link = true;               // If set to false then link output will be return as static path (not all webserver support this so the default is true).
$filescript = 'imgfo-webhook.php';  // If you want to rename the file imgfo-webhook.php, then you have to set this also.
$scheme_link = 'https://';          // The scheme must be set because this script should working without using .htaccess. 
$allow_no_referer = false;          // allow access with no referer policy (default is false).
$allow_domain = [                   // just write the domain or sub domain without scheme and port.
    // 'localhost',                 // localhost should be commented for production use.
    'jsitor.com',                   // for live ajax test with jsitor.com.
    'imgfo.com'
];

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With, Etag');

$imgfo = new ImgFo();

$contentpath = '';
if (!empty($_GET['content'])) {
    $contentpath = rawurldecode($_GET['content']);
    $versioning = (empty($_GET['v'])?'':rawurldecode($_GET['v']));
    $jquery_cache = (empty($_GET['_'])?'':rawurldecode($_GET['_']));
    $expires = (time() + $maxage);
    $etag = '"'.md5($contentpath.$jquery_cache.$versioning).'"';
    header("Cache-Control: public, max-age=".$maxage);
    header("Expires: ".gmdate('D, d M Y H:i:s',$expires)." GMT");
    header('Etag: '.$etag);
    if(isset($_SERVER['HTTP_IF_NONE_MATCH']) && trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) { 
        header("HTTP/1.1 304 Not Modified"); 
        exit;
    }
    if (file_exists($contentpath)) {
        readfile($contentpath);
    } else {
        http_response_code(404);
        echo $imgfo->renderError('File not found!');
    }
} else {
    if (isset($_SERVER["HTTP_REFERER"])) {
        $origin = parse_url($_SERVER["HTTP_REFERER"]);
        if(isset($origin['host'])) {
            if (!in_array($origin["host"],$allow_domain)) {
                http_response_code(403);
                echo $imgfo->renderError('You don\'t have direct access to this service!');
                exit;
            }
        }
    } else {
        if(!$allow_no_referer) {
            http_response_code(403);
            echo $imgfo->renderError('You don\'t have direct access to this service!');
            exit;
        }
    }
    $result = file_get_contents('php://input');
    if( $result) {
        if($dynamic_link) {
            echo $imgfo->renderSuccess('Data saved succesfully!', ["link" => $scheme_link."$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?content=".rawurlencode($imgfo->write($result))]);
        } else {
            echo $imgfo->renderSuccess('Data saved succesfully!', ["link" => str_replace($filescript,"",$scheme_link."$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]").$imgfo->write($result)]);
        }
    } else {
        http_response_code(400);
        echo $imgfo->renderError('Wrong Parameter!');
    }
}

class ImgFo {

    public $filedir = 'imgfo-data'; // please don't change this

    /**
     * Render Success response
     * @return string json
     */
    function renderSuccess($msg, $arrdata) {
        return json_encode([
            'status' => 'true',
            'message' => $msg,
            'data' => $arrdata
        ], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Render Error response
     * @return string json
     */
    function renderError($msg) {
        return json_encode([
            'status' => 'false',
            'message' => $msg
        ], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Generate UUIDv4
     * @return string
     */
    function uuidv4() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    
          // 32 bits for "time_low"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    
          // 16 bits for "time_mid"
          mt_rand(0, 0xffff),
    
          // 16 bits for "time_hi_and_version",
          // four most significant bits holds version number 4
          mt_rand(0, 0x0fff) | 0x4000,
    
          // 16 bits, 8 bits for "clk_seq_hi_res",
          // 8 bits for "clk_seq_low",
          // two most significant bits holds zero and one for variant DCE1.1
          mt_rand(0, 0x3fff) | 0x8000,
    
          // 48 bits for "node"
          mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Write data to file
     * @param string jsonstring     this is the raw json string 
     * @return string link
     */
    function write($jsonstring) {
        $dir = $this->filedir .DIRECTORY_SEPARATOR. gmdate("Y".DIRECTORY_SEPARATOR."m".DIRECTORY_SEPARATOR."d") . DIRECTORY_SEPARATOR;
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
            // create blank index.html for files protection inside directory 
            file_put_contents($dir.DIRECTORY_SEPARATOR.'index.html', '');
        }
        // Create a json file
        $filepath = $dir.$this->uuidv4().'.json';
        file_put_contents($filepath, $jsonstring);
        return str_replace('\\','/',$filepath);
    }

}